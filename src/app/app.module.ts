import { NgModule, ErrorHandler } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { IonicApp, IonicModule, IonicErrorHandler } from 'ionic-angular';
import { MyApp } from './app.component';

import { MapPageModule } from '../pages/map/map.module';
import { ChatPageModule } from '../pages/chat/chat.module';
import { HomePageModule } from '../pages/home/home.module';
import { TabsPage } from '../pages/tabs/tabs';

import { StatusBar } from '@ionic-native/status-bar';
import { SplashScreen } from '@ionic-native/splash-screen';
import { HttpModule } from "@angular/http";

import { UserService } from "../services/user-service";
import { MessageService } from "../services/message-service";
import { ApiInvoker,ProtectedApiInvoker } from "../services/api-invoker";

// Shared Pipes Module
import { PipesModule } from "../pipes/pipes.module";
@NgModule({
  declarations: [
    MyApp,
    TabsPage
  ],
  imports: [
    BrowserModule,
    IonicModule.forRoot(MyApp),
    HttpModule,
    PipesModule,
    MapPageModule,
    ChatPageModule,
    HomePageModule
  ],
  bootstrap: [IonicApp],
  entryComponents: [
    MyApp,    
    TabsPage
  ],
  providers: [
    StatusBar,
    SplashScreen,
    HttpModule,
    UserService,
    MessageService,
    ApiInvoker,
    ProtectedApiInvoker,
    {provide: ErrorHandler, useClass: IonicErrorHandler}
  ]
})
export class AppModule {}
