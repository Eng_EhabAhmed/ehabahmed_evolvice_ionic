import {BaseModel, BaseModelInterface} from "./base-model";

export interface UserInterface extends BaseModelInterface {
  id: number;
  name: string;
}

export class User extends BaseModel implements UserInterface {

  public id: number;
  public name: string; 


  constructor(data: UserInterface | any) {
    super(data);
  }

  protected get dateKeys(): string[] {
    return ['createdAt', 'updatedAt', 'deletedAt'];
  }

  protected get rawKeysMapping(): { [key: string]: string } {
    return {};
  }

  protected postRawDataTransform() {

  }

}
