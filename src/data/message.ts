import {BaseModel, BaseModelInterface} from "./base-model";
import { DateTime } from "ionic-angular/components/datetime/datetime";

export interface MessageInterface extends BaseModelInterface {
  id: number;
  messageText: string; 
  parentMessageId: number;
  createdDate: DateTime; 
  modifiedDate: DateTime;
  createdByUserId: number; 
  senderName: string;
}

export class Message extends BaseModel implements MessageInterface {

  public id: number;
  public messageText: string; 
  public parentMessageId: number;
  public createdDate: DateTime; 
  public modifiedDate: DateTime;
  public createdByUserId: number; 
  public senderName: string; 


  constructor(data: MessageInterface | any) {
    super(data);
  }

  protected get dateKeys(): string[] {
    return ['createdAt', 'updatedAt', 'deletedAt'];
  }

  protected get rawKeysMapping(): { [key: string]: string } {
    return {};
  }

  protected postRawDataTransform() {

  }

}
