import { Component ,ViewChild } from '@angular/core';
import { IonicPage, NavController, NavParams,Content } from 'ionic-angular';
import { MessageService } from "../../services/message-service";
import { Message, MessageInterface } from "../../data/message";
import { User } from '../../data/user';


/**
 * Generated class for the ChatPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-chat',
  templateUrl: 'chat.html',
})
export class ChatPage {
  @ViewChild(Content) content: Content;

  protected messages : MessageInterface [];
  protected page : number = 0;
  protected newMessage : string = "";
  protected users : User[] ;
  constructor(public navCtrl: NavController, public navParams: NavParams, public messageService:MessageService) {
    this.loadMessages()
  }
  loadMessages(){
    this.messageService.getAllMessages(this.page).then((messages : Message[]) => {
      if (messages) {
        console.log(messages);
        this.messages = messages; 
        }       
    });
  }
  getMoreMessages(){
    this.page += 1;
    this.loadMessages();
  }
  sendMessage(){
    var newMessageObj = {"messageText": this.newMessage , "createdDate" : new Date().toISOString() , "modifiedDate" : new Date().toISOString() , "createdByUserId" : 1,"id" : 0 , "parentMessageId":0}
    this.messageService.addMessage(newMessageObj).then((result) => {
      if (result) {
        this.newMessage = "";
        this.loadMessages();        
        setTimeout(()=>{this.content.scrollToBottom();},300);}
    });
  }
 
  ionViewDidLoad() {
    console.log('ionViewDidLoad ChatPage');
  } 
}
