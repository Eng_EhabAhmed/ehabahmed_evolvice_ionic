import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { MessageService } from "../../services/message-service";
import { Message, MessageInterface } from "../../data/message";
/**
 * Generated class for the HomePage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-home',
  templateUrl: 'home.html',
})
export class HomePage {

  protected lastMessage : Message;
  constructor(public navCtrl: NavController, public navParams: NavParams,public messageService:MessageService) {
    this.messageService.getAllMessages(0)
    .then(messages=> this.lastMessage = messages[0]);
    this.messageService.myLastMessage.subscribe((lastMessage: Message) => { this.lastMessage = lastMessage; });
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad HomePage');
    
  }

}
