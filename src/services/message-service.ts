import { Injectable } from "@angular/core";
import { ApiInvoker, ProtectedApiInvoker, StandardResponseInterface } from "./api-invoker";
import { Message, MessageInterface } from "../data/message";
import {Observable} from 'rxjs/Observable';
import { Subject } from 'rxjs/Subject';
import { UserService } from "./user-service";

@Injectable()
export class MessageService  {

  protected messages: Message[] = [];
  protected lastPage : number = 0;
  public myLastMessage: Observable<Message>;
  private lastMessage: Subject<Message>;

  constructor(protected apiInvoker: ProtectedApiInvoker,
    protected publicApiInvoker: ApiInvoker,
    protected userService : UserService) {
      this.lastMessage = new Subject<Message>();
    this.myLastMessage = this.lastMessage.asObservable();
  }

  

  /**
   * Get all messages
   * @returns {Promise<Message[]>}
   */
  getAllMessages(page : number): Promise<Message[]> {
    return (this.messages.length && page <= this.lastPage) ? Promise.resolve(this.messages) : Promise.resolve(this.refreshAllMessages(page))
  }

  /**
   * Always load new messages from the server
   * @returns {Promise<Message[]>}
   */
  refreshAllMessages(page : number): Promise<any> {
    return this.userService.getAllUsers()
    .then(users => this.apiInvoker.invoke('GET', 'Messages/GetMessages/' + page, {}, {}, "")
      .then((messageResponse: any) => JSON.parse(messageResponse).map(message =>{
        message.senderName = users.find(user=> user.id == message.createdByUserId).name;
        return message;
      }))
      .then((newMessagesWithSenderName : any) => this.messages = this.messages.concat(newMessagesWithSenderName)));
    }

  /**
   * Get user by id from the server
   * @returns {Promise<Boolen>}
   */
  addMessage(newMessage): Promise<any> {
      return this.apiInvoker.invoke('POST', 'Messages/AddMessage', {}, {}, newMessage)
      .then(()=> this.setLastMessage(newMessage))
      .then(()=> this.messages.push(newMessage));
    }
  setLastMessage(lastMessage){
      this.myLastMessage = lastMessage;
      this.lastMessage.next(lastMessage);
    }
}
