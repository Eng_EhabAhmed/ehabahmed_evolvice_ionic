import {Injectable} from '@angular/core';
import {Headers, Http, RequestOptions} from '@angular/http';
import 'rxjs/add/operator/map';
import { Events } from 'ionic-angular';

export interface StandardResourceResponse {
  data: any;
}

@Injectable()
export class ApiInvoker {

  protected static ApiBaseUrl: string = 'http://localhost:53130/api/';

  constructor(protected http: Http) {
  }


  public invoke(method, url, queryString = {}, headers = {}, data): Promise<StandardResourceResponse> {
      let hdrs: Headers = new Headers();

      return new Promise((resolve, reject) => {
        return this.http.request(ApiInvoker.ApiBaseUrl + url, {
          method: method,
          headers: hdrs,
          body: data,
          params: queryString
        } as RequestOptions).subscribe(response => resolve(response.json())), error => reject(error);
      }); 
  }
}

@Injectable()
export class ProtectedApiInvoker extends ApiInvoker {

  constructor(http: Http) {
    super(http);
  }

  public invoke(method, path, queryString = {}, headers = {}, data): Promise<StandardResourceResponse> {
    return super.invoke(method, path, queryString, {}, data);
  }


}

export interface StandardResponseInterface {
  data: any;
}