import { Injectable } from "@angular/core";
import { ApiInvoker, ProtectedApiInvoker, StandardResponseInterface } from "./api-invoker";
import { User, UserInterface } from "../data/user";

@Injectable()
export class UserService  {

  protected users: User[] = [];


  constructor(protected apiInvoker: ProtectedApiInvoker,
    protected publicApiInvoker: ApiInvoker) {
  }

  /**
   * Get one user by Id
   * @returns {Promise<User>}
   */
  getUserById(id) : Promise<User> {
    return this.getAllUsers()
    .then(users => users.find(obj => {
      return obj.id == id;
    }))    
  }

  /**
   * Get all users
   * @returns {Promise<User[]>}
   */
  getAllUsers(): Promise<User[]> {
    return this.users.length ? Promise.resolve(this.users) : this.refreshAllUsers();
  }

  /**
   * Always load new users from the server
   * @returns {Promise<User[]>}
   */
  refreshAllUsers(): Promise<User[]> {
    return this.apiInvoker.invoke('GET', 'Users/GetUsers', {}, {}, "")
      .then((userResponse: any) => this.users = JSON.parse(userResponse))

    }

  /**
   * Get user by id from the server
   * @returns {Promise<User>}
   */
  refreshUserById(id : number): Promise<any> {
      return this.apiInvoker.invoke('GET', 'Users/GetUserById/' + id, {}, {}, "")
      .then((user : any)=> this.users.push(user));
    }

}
