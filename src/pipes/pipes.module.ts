import { NgModule } from "@angular/core";

// Pipes
import { TimeAgoPipe } from "./timeago";
import { SortPipe } from "./sort";

@NgModule({
  declarations: [TimeAgoPipe,SortPipe],
  imports: [],
  exports: [TimeAgoPipe,SortPipe]
})
export class PipesModule {}
