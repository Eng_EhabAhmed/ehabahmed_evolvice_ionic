import { Pipe, PipeTransform } from '@angular/core';
import * as moment from 'moment'; 

@Pipe({
  name: 'timeago',
  pure: false
})

export class TimeAgoPipe implements PipeTransform {
    transform(value: any): any{
        return moment(value).fromNow(); 
    }
}
