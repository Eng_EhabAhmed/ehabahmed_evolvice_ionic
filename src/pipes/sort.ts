import { Pipe, PipeTransform } from '@angular/core';
import * as _ from 'lodash'; 

@Pipe({
  name: 'sort',
  pure: false
})

export class SortPipe implements PipeTransform {
    transform(value: any, sortBy:string): any{
        if(!value) {
            return value;
        }

        if(sortBy.startsWith('-')){
            var sortB = sortBy.replace('-', '')
            var arr = _.sortBy(value, sortB);
            return _.reverse(arr);
        } else {
            return _.sortBy(value, sortBy);
        }
    }
}
